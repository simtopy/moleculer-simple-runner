import { ServiceBroker } from "moleculer"
import IPCemitter from "./IPCemitter.js"

/**
 * @private
 * @external ServiceBroker
 * @see {@link https://moleculer.services/docs/0.14/broker.html}
 */

/**
 * @private
 * @external Context
 * @see {@link https://moleculer.services/docs/0.14/context.html}
 */

/**
 * Callback for handling moleculer events
 * @private
 * @callback EventHandler
 * @param {Context} ctx - the event context 
 */

/**
 * 
 */

/**
 * @class Runner - an api the the ServiceBroker
 * @param {ServiceBroker|IPCemitter|Object} conf
 */
class Runner {

  /* beautify preserve:start */
  // properties
  #started
  #namespace
  #nodeID
  #services
  // methods
  #log
  #start
  #stop
  #kill
  #call
  #mcall
  #emit
  #broadcast
  #onEvent
  #onceEvent
  #waitForServices
  /* beautify preserve:end */

  constructor(conf) {

    if (conf instanceof ServiceBroker) {

      // Broker-based configuration       
      let broker = conf

      this.#nodeID = broker.nodeID
      this.#namespace = broker.namespace
      this.#started = broker.started
      this.#services = broker.services

      this.#start = () => broker.start()
      this.#stop = () => broker.stop()
      this.#kill = () => this.#stop().then(() => process.kill(process.pid))
      this.#log = (level, ...args) => broker.logger[level](...args)
      this.#call = (...args) => broker.call(...args)
      this.#mcall = (actions) => broker.mcall(actions)
      this.#emit = (...args) => broker.emit(...args)
      this.#broadcast = (...args) => broker.broadcast(...args)
      this.#onEvent = (callback) => broker.onEvent(callback)
      this.#onceEvent = (eventName, callback) => broker.onceEvent(eventName, callback)
      this.#waitForServices = (serviceNames, timeout, interval) => broker.waitForServices(serviceNames, timeout, interval)

    } else if (conf instanceof IPCemitter) {
      // IPCemitter-based configuration

      let emitter = conf

      emitter.on("forwardEvent", ({ data }) => {
        emitter.emit("event")
        emitter.emit(data.eventName, data)
      })

      let ready = new Promise(resolve => emitter.once("ready", () => Promise.all([
        emitter.request(`broker.property`, "nodeID").then(nodeID => this.#nodeID = nodeID),
        emitter.request(`broker.property`, "namespace").then(namespace => this.#namespace = namespace)
      ]).then(resolve)))

      this.#start = () => ready.then(() => emitter.request(`broker.start`).then(() => this.#started = true))
      this.#stop = () => emitter.request(`broker.stop`).then(() => this.#started = false)
      this.#kill = () => this.#stop().then(() => emitter.kill())
      this.#log = (level, ...args) => emitter.request(`broker.log`, { level, args })
      this.#call = (action, params, options) => emitter.request(`broker.call`, { action, params, options })
      this.#mcall = (actions) => emitter.request(`broker.mcall`, actions)
      this.#emit = (event, payload, options) => emitter.request(`broker.emit`, { event, payload, options })
      this.#broadcast = (event, payload, options) => emitter.request(`broker.broadcast`, { event, payload, options })
      this.#onEvent = (callback) => emitter.on("event", callback)
      this.#onceEvent = (eventName, callback) => emitter.once(eventName, callback)
      this.#waitForServices = (serviceNames, timeout, interval) => emitter.request(`broker.waitForServices`, { serviceNames, timeout, interval })

    } else {
      // Object-based configuration       
      console.log("Custom Configuration not implemented", conf)
    }
  }

  get started() {
    return this.#started
  }

  get namespace() {
    return this.#namespace
  }

  get nodeID() {
    return this.#nodeID
  }

  get services() {
    if (!this.#services) throw Error(`services are only available with a local runner`)
    return this.#services
  }

  /**
   * Start the broker
   *
   * @param {number} [timeout=0]
   * @return {Promise<>}    
   */
  start(timeout = 0) {
    return this.#start()
  }

  /**
   * Stop the broker
   *
   * @param {number} [timeout=0]
   * @return {Promise<>} 
   */
  stop(timeout = 0) {
    return this.#stop()
  }

  /**
   * Kill the process of the broker
   * @returns 
   */
  kill() {
    return this.#kill()
  }

  /**
   * Calls logger.info
   * @param  {...any} args 
   */
  info(...args) {
    this.#log("info", ...args)
  }

  /**
   * Calls logger.warn
   * @param  {...any} args 
   */
  warn(...args) {
    this.#log("warn", ...args)
  }

  /**
   * Calls logger.debug
   * @param  {...any} args 
   */
  debug(...args) {
    this.#log("debug", ...args)
  }

  /**
   * Calls logger.error
   * @param  {...any} args 
   */
  error(...args) {
    this.#log("error", ...args)
  }

  repl() {

  }

  /**
   * Adds the handler function to event listerner
   * (Will fire on all events)
   * @param {EventHandler} callback 
   */
  onEvent(callback) {
    this.#onEvent(callback)
  }

  /**
   * Adds the handler function to event listerner
   * (Will fire once, on eventName)
   * @param {string} eventName 
   * @param {EventHandler} callback 
   */
  onceEvent(eventName, callback) {
    this.#onceEvent(eventName, callback)
  }

  /**
   * Wait for services to be started
   * @param {string[]} serviceNames 
   * @param {integer} timeout 
   * @param {integer} interval 
   */
  waitForServices(serviceNames, timeout, interval) {
    return this.#waitForServices(serviceNames, timeout, interval)
  }

  /**
   * Call an action
   * @param {string} action - service action to call
   * @param {any} [params=null] - params of moleculer call
   * @param {any} [opts=Object] - options of moleculer call
   * @return {Promise<any>} - result of the call 
   */
  call(actionName, params = null, opts = {}) {
    return this.#call(actionName, params, opts)
  }

  /**
   * Call multiple actions   
   * @param {Object[]} actions 
   * @returns {Promise<any>[]}
   */
  mcall(actions) {
    return this.#mcall(actions)
  }

  /**
   * Emit an event
   * @param {string} action - service action to call
   * @param {any} [params=null] - params of moleculer call
   * @param {any} [opts=Object] - options of moleculer call
   * @return {Promise<any>} - result of the call 
   */
  emit(eventName, payload, opts) {
    return this.#emit(eventName, payload, opts)
  }

  /**
   * Broadcast an event
   * @param {string} action - service action to call
   * @param {any} [params=null] - params of moleculer call
   * @param {any} [opts=Object] - options of moleculer call
   * @return {Promise<any>} - result of the call 
   */
  broadcast(eventName, payload, opts) {
    return this.#broadcast(eventName, payload, opts)
  }
}

export { Runner }