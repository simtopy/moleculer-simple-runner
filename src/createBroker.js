import dotenv from "dotenv"
import { EventEmitter } from "events"
import { ServiceBroker } from "moleculer"
import { findServicesInScope } from "./utils/findServices.js"
import { validateService } from "./utils/validateService.js"
import { inspect } from "util"

let emitter = new EventEmitter()

class RunnerBroker extends ServiceBroker {

  constructor(...args) {
    super(...args)
  }

  onEvent(callback) {
    emitter.on("event", callback)
  }

  onceEvent(eventName, callback) {
    emitter.once(eventName, callback)
  }
}

const runnerService = {
  name: "$runner",
  events: {
    "**": function(ctx) {
      emitter.emit("event", ctx)
      emitter.emit(ctx.eventName, ctx)
    }
  }
}

/**
 * @typedef {object} Config
 * @property {boolean?} config.MOLECULER_REPL_MODE - enter repl mode on 
 * @property {string?} config.SERVICES_DIR - directories to explore for services, separated by ":"
 * @property {string?} config.SERVICES - services in previous directories to exec, will enable whitelist mode
 * @property {string?} config.EXCLUDED_SERVICES - service to exclude from execution
 * @property {string?} config.BROKER_CONFIG - configuration of the broker
 * @property {string|object|null} config.RUNNER_ENV - env file or object
 * @property {object|null} config.RUNNER_ENV_OVERRIDES - additionnal environment
 * @property {Object} config.options
 * @property {boolean} config.options.verbose - enable logging
 */

/**
 *
 * Run a moleculer broker with given config
 *
 * @export
 * @param {Config} [config={}] - 
 * 
 * @return {RunnerBroker} 
 */
export default async function(config = {}) {

  //verbose
  const verbose = config.verbose || (process.env.VERBOSE_RUNNER === "true")

  const pid = process.pid

  /*
   * Environment Management
   */
  const env = config.RUNNER_ENV ? config.RUNNER_ENV : {}
  const additionnalEnv = config.RUNNER_ENV_OVERRIDES ? config.RUNNER_ENV_OVERRIDES : {}

  let logEnv = null
  if (typeof env === "string") logEnv = dotenv.config({ path: env })
  else if (typeof env === "object") {
    logEnv = env
    Object.assign(process.env, env)
  }
  if (typeof additionnalEnv === "object")[
    Object.assign(process.env, additionnalEnv)
  ]

  /*
   * Config Management
   */
  const repl = config.MOLECULER_REPL_MODE ? config.MOLECULER_REPL_MODE : process.env.MOLECULER_REPL_MODE ? process.env.MOLECULER_REPL_MODE : false
  const path = config.SERVICES_DIR ? config.SERVICES_DIR : process.env.SERVICES_DIR ? process.env.SERVICES_DIR : ""
  const included = config.SERVICES ? config.SERVICES : process.env.SERVICES ? process.env.SERVICES : ""
  const excluded = config.EXCLUDED_SERVICES ? config.EXCLUDED_SERVICES : process.env.EXCLUDED_SERVICES ? process.env.EXCLUDED_SERVICES : ""
  const brokerConfig = config.BROKER_CONFIG ? config.BROKER_CONFIG : process.env.BROKER_CONFIG ? process.env.BROKER_CONFIG : ""

  /* 
   * Broker creation
   */
  let brokerOptions = {}
  if (brokerConfig !== "") {
    brokerOptions = await import(`${process.cwd()}/${brokerConfig}`).then(m => {
      switch (typeof m.default) {
        case "function":
          return m.default()
        case "object":
          return m.default
        default:
          throw Error(`${process.cwd()}/${brokerConfig} should export a config object or a function`)
      }
    })
  } else {
    console.warn(`[RUNNER ${pid}] No broker config was given to runner, this is most likely a mistake.`)
  }

  let broker = new RunnerBroker(brokerOptions)

  //Log config stuff
  if (verbose) broker.logger.info(`broker options loaded from ${brokerConfig}`, brokerOptions)
  if (verbose) broker.logger.info(`Starting bootstrap sequence with config :`)
  if (verbose) broker.logger.info(` - SERVICES_DIR : ${path}`)
  if (verbose) broker.logger.info(` - SERVICES : ${included}`)
  if (verbose) broker.logger.info(` - EXCLUDED_SERVICES : ${excluded}`)
  if (verbose) broker.logger.info(` - BROKER_CONFIG : ${brokerConfig}`)
  if (verbose) broker.logger.info(` - MOLECULER_REPL_MODE : ${repl}`)
  if (verbose) broker.logger.info(` - RUNNER BASE ENV : ${inspect(env, false, 10, true)}`)
  if (verbose) broker.logger.info(` - RUNNER OVERRIDED ENV : ${inspect(additionnalEnv, false, 10, true)}`)

  //Import .services.js files from "path"
  if (verbose) broker.logger.info(` Starting searching for services`)
  const services = await Promise.all(path.split(':').map(p => findServicesInScope(p, verbose ? broker.logger : null)))
    .then(imports => Promise.all(imports.flat()))
    .then(modules => {
      return modules.map(m => m.default)
      // filters here !
    })

  if (services.length === 0) {
    broker.logger.warn(`No service were found, please check the given path`)
  } else {
    if (verbose) broker.logger.info(`${services.length} services were found, creating services...`)
  }

  let toRun = []

  //filter services
  if (included !== "") {
    const allowed = included.split(",").map(s => s.toLowerCase())
    if (verbose) broker.logger.info(`running only services allowed for running : ${allowed}`)
    toRun = services.filter(r => allowed.includes(r.name.toLowerCase()))
  } else {
    const forbidden = excluded === "" ? [] : excluded.split(",").map(s => s.toLowerCase())
    if (verbose) broker.logger.info(`exclude forbidden services for running: ${forbidden}`)
    toRun = services.filter(r => !forbidden.includes(r.name.toLowerCase()))
  }

  //Validate services
  toRun.forEach(validateService);

  //Create the service  
  [runnerService, ...toRun].forEach(r => {
    if (verbose) broker.logger.info(`Creating service : ${r.name}`)
    broker.createService(r)
  })

  if (repl) {
    if (verbose) broker.logger.info(`Finished Moleculer Initialization, entering the repl`)
    broker.repl()
  }

  return broker
}