import { v4 } from "uuid"
import EventEmitter from "events";
import * as util from "util"

export default class IPCemitter extends EventEmitter {
  constructor(process) {
    super()
    this.process = process
    this.process.on("message", ({ event, msg }) => {
      const { data = null, resUid = null } = msg
      this.emit(event, { data, resUid })
    })
  }

  kill() {
    this.process.kill()
  }

  send(event, data = null, resUid = null) {
    this.process.send({
      event,
      msg: {
        data,
        resUid
      }
    })
  }

  request(event, data = null) {
    return new Promise((resolve, reject) => {
      let resUid = v4()
      this.send(event, data, resUid)
      this.once(resUid, response => resolve(response.data))
    })
  }

  onRequest(event, handler) {
    this.on(event, (requestEvent) => {
      if (!requestEvent.resUid) throw Error(`NO resEvent uid : ${event}, ${util.inspect(requestEvent, false, 10, true)}`)
      const ans = handler(requestEvent.data)
      this.send(requestEvent.resUid, ans)
    })
  }

  onRequestAsync(event, handler) {
    this.on(event, (requestEvent) => {
      if (!requestEvent.resUid) throw Error(`NO resEvent uid : ${event}, ${util.inspect(requestEvent, false, 10, true)}`)
      handler(requestEvent.data).then(ans => {
        this.send(requestEvent.resUid, ans)
      })
    })
  }
}