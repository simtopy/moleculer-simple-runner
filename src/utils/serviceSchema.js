export default {
  type: "object",
  properties: {
    name: {
      type: "string",
    },
    dependencies: {
      anyOf: [{
          type: "string"
        },
        {
          type: "array"
        }
      ],
    },
    version: {
      anyOf: [{
          type: "string"
        },
        {
          type: "number"
        }
      ]
    },
    actions: {
      type: "object"
    },
    methods: {
      type: "object"
    },
    events: {
      type: "object"
    },
    settings: {
      type: "object"
    },
    mixins: {
      type: "array"
    },
    merged: {},
    started: {},
    created: {},
    stopped: {}
  },
  required: ["name"],
  additionalProperties: false
}