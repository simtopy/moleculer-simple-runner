import { promises as Fs } from "fs"

export async function findServicesInScope(path, logger) {
  const dir = await Fs.opendir(path).then(res => {
    if (logger) logger.info(`Exploring directory ${path} for services`)
    return res
  }).catch(err => {
    console.error(`[RUNNER ${process.pid}] Cannot open ${path} : ${err}`)
  })

  const res = []
  for await (const dirent of dir) {
    if (dirent.isFile()) {
      if (dirent.name.includes("service.js")) {
        if (logger) logger.info(`found service in : ${path}`)
        const service = import(`${process.cwd()}/${dir.path}/${dirent.name}`)
        res.push(service)
      }
    }
    if (dirent.isDirectory()) {
      res.push(...(await findServicesInScope(`${dir.path}/${dirent.name}`, logger)))
    }
  }

  return res
}