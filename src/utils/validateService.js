import Util from "util"
import Ajv from "ajv"
import serviceSchema from "./serviceSchema.js"

const ajv = new Ajv.default()
const validate = ajv.compile(serviceSchema)

export function validateService(service) {
  const res = validate(service)
  if (!res) {
    throw new Error(`Invalid service : ${service.name}, ${Util.inspect(validate.errors)}`)
  }
  return res
}