export default {
  name: "service1",
  events: {
    "testEvent": function(ctx) {
      this.broker.emit("testResponse", ctx.params + 1)
    }
  },
  actions: {
    doStuff(ctx) {
      return ctx.params
    }
  },
}