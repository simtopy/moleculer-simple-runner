import * as uuid from "uuid"

export default {
  nodeID: process.env.NODE_ID,
  namespace: "runner-tests",
  // Number of seconds to wait before setting node to unavailable status.
  heartbeatTimeout: 1,
  // Number of seconds to send heartbeat packet to other nodes.
  heartbeatInterval: 1,
  dependencyInterval: 1,

  uidGenerator: uuid.v4,

  transporter: `redis://${process.env.TRANSPORTER_HOST || "localhost"}:${process.env.TRANSPORTER_PORT || 6379}`,

  // Logger
  logger: {
    type: "Console",
    options: {
      level: process.env.LOG_LEVEL || "info",
      formatter: "{nodeID} {level} {mod} {msg}"
    }
  },

  //Disable built-in request & emit balancer.
  disableBalancer: false,
  // Settings of Service Registry (disabled)
  registry: {
    strategy: "RoundRobin",
    preferLocal: true,
    discoverer: {
      type: "Local",
      options: {
        // Use broker heartbeat
        heartbeatInterval: null,

        // Use broker heartbeat timeout
        heartbeatTimeout: null,

        // Disable heartbeat checking & sending, if true
        disableHeartbeatChecks: false,

        // Disable removing offline nodes from registry, if true
        disableOfflineNodeRemoving: false,

        // Remove offline nodes timeout (seconds)
        cleanOfflineNodesTimeout: 1
      }
    }
  },
}