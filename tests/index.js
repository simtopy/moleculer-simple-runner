import { strict as Assert } from "assert"
import createRunner from "../index.js"

const config = {
  verbose: false,
  RUNNER_ENV: "tests/config/.env",
  BROKER_CONFIG: "tests/config/broker.js"
}

describe("test the runner", function() {
  this.timeout(10000)

  var runner

  before(async function() {
    process.env.SERVICES_DIR = "./tests/services"
    process.env.SERVICES = "service1"
  })

  it("creates a local runner", async function() {
    process.env.SERVICES = "service1"
    runner = await createRunner(config)
  })

  it("start a local runner", function() {
    return runner.start()
  })

  it("calls runner.info", function() {
    runner.info("Hello world !")
  })

  it("calls local runner.call", async function() {
    let n = await runner.call("service1.doStuff", 42)
    Assert.equal(n, 42)
  })

  it("calls local runner.mcall", async function() {
    let [res1, res2, res3] = await Promise.all([
      runner.call("service1.doStuff", 1),
      runner.call("service1.doStuff", 2),
      runner.call("service1.doStuff", 3)
    ])

    Assert.equal(res1, 1)
    Assert.equal(res2, 2)
    Assert.equal(res3, 3)
  })

  it("calls local runner.emit", function(done) {
    runner.onceEvent("testResponse", ctx => {
      Assert.equal(ctx.params, 1)
      done()
    })
    runner.emit("testEvent", 0)
  })

  it("calls local runner.broadcast", function(done) {
    runner.onceEvent("testResponse", ctx => {
      Assert.equal(ctx.params, 1)
      done()
    })
    runner.broadcast("testEvent", 0)
  })

  it("stop a local runner", function() {
    return runner.stop()
  })

  it("start a forked runner", async function() {
    runner = await createRunner(config, "forked-node")
    return runner.start()
  })

  it("has the correct nodeID", function() {
    Assert.equal(runner.nodeID, "forked-node")
  })

  it("calls forked runner.call", async function() {
    let n = await runner.call("service1.doStuff", 42)
    Assert.equal(n, 42)
  })

  it("calls forked runner.mcall", async function() {
    let [res1, res2, res3] = await Promise.all([
      runner.call("service1.doStuff", 1),
      runner.call("service1.doStuff", 2),
      runner.call("service1.doStuff", 3)
    ])

    Assert.equal(res1, 1)
    Assert.equal(res2, 2)
    Assert.equal(res3, 3)
  })

  it("calls forked runner.emit", function(done) {
    runner.onceEvent("testResponse", ctx => {
      Assert.equal(ctx.params, 1)
      done()
    })
    runner.emit("testEvent", 0)
  })

  it("calls forked runner.broadcast", function(done) {
    runner.onceEvent("testResponse", ctx => {
      Assert.equal(ctx.params, 1)
      done()
    })
    runner.broadcast("testEvent", 0)
  })

  it("stop a forked runner", function() {
    return runner.stop()
  })

  it("kills a forked runner", function() {
    return runner.kill()
  })

  it("test verbose", async function() {
    runner = await createRunner(Object.assign({ verbose: true }, config), "verbose-fork")
    await runner.start()
    await runner.stop()
  })

  it("run multiple forks", async function() {

    let runners = await Promise.all([
      "forked-node-1", "forked-node-2", "forked-node-3"
    ].map(forkedID => createRunner(Object.assign({ SERVICES: "service3" }, config), forkedID)))

    await Promise.all(runners.map(f => f.start()))
    await Promise.all(runners.map(f => f.call("service3.doStuff")))
    await Promise.all(runners.map(f => f.stop()))
  })

  it("communication betweens forked brokers", async function() {

    let runner1 = await createRunner(Object.assign({ SERVICES: "service1" }, config), true)

    let runner2 = await createRunner(Object.assign({ SERVICES: "service2" }, config), true)

    await Promise.all([runner1.start(), runner2.start()])

    await runner1.call("service2.doStuff")

    await Promise.all([runner1.stop(), runner2.stop()])
  })

  it("waitForServices", async function() {

    let runner1 = await createRunner(Object.assign({ SERVICES: "service1" }, config), "service1")
    let runner2 = await createRunner(Object.assign({ SERVICES: "service2" }, config), "service2")

    await runner2.start()

    let waitForService1 = runner2.waitForServices("service1")
    Assert.ok(waitForService1 instanceof Promise)

    runner1.start()
    await waitForService1

    await Promise.all([runner1.stop(), runner2.stop()])

  })
})