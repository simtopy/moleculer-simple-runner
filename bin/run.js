#!/usr/bin/env node

import createRunner from "../index.js"

createRunner().then(runner => runner.start())