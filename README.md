# moleculer-simple-runner

A simple runner for **moleculer**, designed to easily test applications in a **multi-process environment** to trooblesoot message brokers and system configuration

## Purpose

We make an intensive use of **moleculer** in our project Simtopy as its design allows flexibility and scalability. To build a strong test policy, we needed to be as close as a real production environment where the microservices are isolated each in their own container, but we still wanted something more lightweight and handy than running containers for integration tests.
Therefore we developped this runner to fit our needs regarding these matters. it allows to isolate each microservice in its own process by using forks.

In addition, we wanted something more lighweight and simple than the full featured moleculer-moleculer to start our service in containers in production environment, so we provided this runner with both a fork and non-fork mode.

## Maintainers

*   Lucas Vincent
*   Ewen Marechal

## Licence

This software is distributed under MIT Licence, feel free to share, modify, fork, reuse of contribute

## Quickstart

### As a standalone process

```bash
npm install moleculer-simple-runner

SERVICES="service1,service2",
SERVICES_DIR="my_services_dir_1:my_services_dir_2"
MOLECULER_REPL_MODE="true" # useful when you start the runner in a dev/pre-prod environment for various audit and testing

run-moleculer
```

### Within a script

```js
import createRunner from "moleculer-simple-runner"

const runnerOptions = {
  SERVICES="service1,service2",
  SERVICES_DIR="my_services_dir_1:my_services_dir_2"
}

// create the broker on the scipt process
let localRunner = await createRunner(runnerOptions)

// create the broker on a forked process
let forkedRunner = await createRunner(runnerOptions, "fork-process-nodeID") // set the second argument to true to used the process pid as a nodeID

// start the broker through the runner api
await localRunner.start()
```

## Configuration

Environement variables / config string :\
MOLECULER_REPL_MODE:\
If set to anything else than a false value, will enter the moleculer repl after the broker start\
SERVICES_DIR:\
One or many directories to search for moleculer services, separated by ":"\
SERVICES :\
whitelist of modules to run, separated by a comma\
EXCLUDED-SERVICES:\\
blacklist of services, separated by a comm\
BROKER_CONFIG: \
path to the broker js config object module\
RUNNER_ENV :\
either a string path to an env file (see dotenv), or a js object to set env variables
RUNNER_ENV_OVERRIDES:\
Overrides the previous env if necessary (when using a base conf + specific conf for services)
